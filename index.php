<div class="container">
    <h1>Загрузка изображения с изменением размеров</h1>
    <form method="post" enctype="multipart/form-data" action="upload.php">
        <input type="file" name="picture">
        <br>
        <input type="submit" value="Загрузить">
        <br>
    </form>
</div>

<?php
require_once './upload.php';
$images = scandir(realpath($images_dir)); // Получаем список файлов из этой директории
$preview = scandir(realpath($preview_dir)); // Получаем список файлов из этой директории
/* Дальше происходит вывод изображений на страницу сайта (по 4 штуки на одну строку) */
?>
<h1>Изображения</h1>
<?php for ($i = 0; $i < count($images); $i++) { ?>
    <a href="<?= '/images/' . $images[$i] ?>" target="_blank">
        <img class="img__mini" src="<?= '/images/' . $images[$i] ?>" alt=""/>
    </a>
    <?php if (($i + 1) % 4 == 0) { ?><br/><?php } ?>
<?php } ?>

<h1>Превью</h1>
<?php for ($i = 0; $i < count($preview); $i++) { ?>
    <a href="<?= '/preview/' . $preview[$i] ?>" target="_blank">
        <img class="img__mini" src="<?= '/preview/' . $preview[$i] ?>" alt=""/>
    </a>
    <?php if (($i + 1) % 4 == 0) { ?><br/><?php } ?>
<?php } ?>
