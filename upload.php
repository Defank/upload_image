<?php

// Путь загрузки
$images_dir = __DIR__ . '/images/';
// Путь до папки с превью
$preview_dir = __DIR__ . '/preview/';

// Обработка запроса
$upload_file = $_FILES['picture']['tmp_name'];
$upload_name = $_FILES['picture']['name'];

// созданине директорий
if (!file_exists($images_dir)) {
    mkdir($images_dir);
}
// созданине директорий
if (!file_exists($preview_dir)) {
    mkdir($preview_dir);
}

// обработка запроса
if (isset($upload_file)) {
    // расширение картинки
    $file_ext = array_pop(explode('.', $upload_name));
    // путь до изображения
    $upload_file_path = $images_dir . uniqid() . '.' . $file_ext;
    // путь до превью
    $preview_file_path = $preview_dir . uniqid() . '.' . $file_ext;

    // загрузка файла
    if (move_uploaded_file($upload_file, $upload_file_path)) {
        // ресайз загруженного изображения
        resize($upload_file_path, $preview_file_path, 100, 100);
        header("Location: " . $_SERVER['HTTP_REFERER']);
        exit;
    } else {
        exit('Ошибка. Файл не загружен');
    }
}


/**
 * $image - путь до фалйа
 * $w_o и h_o - ширина и высота выходного изображения
 * @param $input_image
 * @param $output_image
 * @param bool $w_o
 * @param bool $h_o
 * @return bool
 */
function resize($input_image, $output_image, $w_o = false, $h_o = false)
{
    if (($w_o < 0) || ($h_o < 0)) {
        echo "Некорректные входные параметры";
        return false;
    }
    list($w_i, $h_i, $type) = getimagesize($input_image); // Получаем размеры и тип изображения (число)
    $types = array("", "gif", "jpeg", "png"); // Массив с типами изображений
    $ext = $types[$type]; // Зная "числовой" тип изображения, узнаём название типа
    if ($ext) {
        $func = 'imagecreatefrom' . $ext; // Получаем название функции, соответствующую типу, для создания изображения
        $img_i = $func($input_image); // Создаём дескриптор для работы с исходным изображением
    } else {
        echo 'Некорректное изображение'; // Выводим ошибку, если формат изображения недопустимый
        return false;
    }
    /* Если указать только 1 параметр, то второй подстроится пропорционально */
    if (!$h_o) $h_o = $w_o / ($w_i / $h_i);
    if (!$w_o) $w_o = $h_o / ($h_i / $w_i);
    $img_o = imagecreatetruecolor($w_o, $h_o); // Создаём дескриптор для выходного изображения
    imagecopyresampled($img_o, $img_i, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i); // Переносим изображение из исходного в выходное, масштабируя его
    $func = 'image' . $ext; // Получаем функция для сохранения результата
    return $func($img_o, $output_image); // Сохраняем изображение в тот же файл, что и исходное, возвращая результат этой операции
}